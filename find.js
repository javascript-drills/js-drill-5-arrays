function find(elements, cb) {
    if(Array.isArray(elements)){
        for( let index=0; index<elements.length; index++ ){
            let res = cb(elements[index]);
    
            if( res == true){    
                return elements[index];
            }
        }
        return undefined;
    }
    return;
}

module.exports = find;