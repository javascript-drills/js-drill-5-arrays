const { items } = require("../dataSetArray.js");
const reduce = require("../reduce.js");

let reduceAns;
if(Array.isArray(items)){
    reduceAns = reduce(items, (accumulator, currentValue) => {
        return accumulator += currentValue;
    }, 0);
    console.log(reduceAns);
}
else{
    console.log("Wrong data passed.");
}