const {items} = require("../dataSetArray");
const map = require("../map");

const result = map(items, (element) => {
    return element * 2;
});

if(result == undefined){
    console.log("Wrong data passed.");
}
else{
    console.log(result);
}