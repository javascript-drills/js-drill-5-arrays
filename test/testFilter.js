const { items } = require("../dataSetArray");
const filter = require("../filter.js");

const filteredArray = filter(items, callbackFunc => {
    return callbackFunc > 2;
});
if(filteredArray){
    console.log(filteredArray);
}
else{
    console.log("Wrong data passed.");
}