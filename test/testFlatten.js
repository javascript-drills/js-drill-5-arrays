const {nestedArray} = require("../dataSetNestedArray");
const flatten = require("../flatten.js");

const flattenedArray = flatten(nestedArray);

if(flattenedArray){
    console.log(flattenedArray);
}
else{
    console.log(" Wrong data passed. ");
}