function filter(elements, cb) {
    const filteredArray = [];
    if(Array.isArray(elements)){
        for( let index=0; index<elements.length; index++){
            if(cb(elements[index]) === true){
                filteredArray.push(elements[index]);
            }
        }
        return filteredArray;
    }
    return;
}

module.exports = filter;