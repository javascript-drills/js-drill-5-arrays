function getflatArray(elements, flatArr){
    for( let index=0; index<elements.length; index++ ){
        if(Array.isArray(elements[index])){
            getflatArray(elements[index], flatArr);
        }
        else{
            flatArr.push(elements[index]);
        }
    }
}

function flatten(elements) {
    const flatArr = [];
    if(Array.isArray(elements)){
        getflatArray(elements, flatArr);
        
        return flatArr;
    }
    return;
}
module.exports = flatten;