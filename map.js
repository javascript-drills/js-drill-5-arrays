function map(elements, cb) {
    const res = [];
    
    if(Array.isArray(elements)){
        for(let element of elements) {
            let ans = cb(element);
            res.push(ans);
        }
        return res;
    }
    return;
}

module.exports = map;